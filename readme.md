#ToDo - Prova Técnica - Alexandre Feustel Baehr    

Requisitos:  
1) Composer;  
2) NodeJS + NPM + Bower;  
3) Apesar da especificação conter PHP 5.3, para esta tarefa será utilizado PHP >=5.6.4;  
4) MySQL 5.5.  

Ferramentas/frameworks utilizados:  
1) Laravel 5.3 (https://laravel.com/docs/5.3);  
2) MySQL 5.5;  
3) Postman;  
4) JQuery;  
5) CSS.    

Instruções:    

1) Clonar o projeto  
```bash
$ git clone https://alebae@bitbucket.org/alebae/todo-alexandre.git  
```

2) Navegar até a pasta criada  
```bash
$ cd todo-alexandre  
```

3) Executar o Composer   
```bash
$ composer install 
``` 

4) Executar o Bower  
```bash
$ bower install  
``` 

6) Crie um banco de dados  
```sql
CREATE DATABASE `todo-alexandre` /*!40100 COLLATE 'utf8_general_ci' */;
```   

5) Rodar as migrations para gerar as tabelas do banco de dados  
```bash
$ php artisan migrate  
``` 

6) Habilitar o servidor embutido no PHP.  
```bash
$ php artisan serve  
```

6) Utilizar algum plugin REST Client como o Postman ou RESTClient        

   a) Escolha o método POST para a URL http://localhost:8000/todo (Sendo 8000 a porta que for indicada no Console, no momento da habilitação do servidor PHP);  
   ![alt tag](http://i.imgur.com/k7TrRFC.jpg)    

   b) Na aba Headers, escolha a key (Content-Type) e o value (application/x-www-form-urlencoded);  

   c) Na aba Body, adicione a key (title) e o value (Tarefa1), por exemplo. Adicione mais uma key (description) e o seu respectivo value (Descrição da tarefa), por exemplo. Clique em Send;  
   ![alt tag](http://i.imgur.com/9Et3D0G.jpg)    

   d) Para a edição, basta alterar o método para PUT, alterar a url para http://localhost:8000/todo/1 ou outra ID criada e alterar os values da aba Body e clicar em Send;    

   e) Para a exclusão, altere o método para DELETE e utilize a mesma URL que foi utilizada para editar.    

   f) Para visualizar, trocar a priorização de tarefas (drag'n drop) utilize no seu navegador a URL http://localhost:8000/todo