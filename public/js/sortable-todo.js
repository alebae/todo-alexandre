$(function() {
        $('.sortable').sortable({
            handle: 'li'
        })
        .on('sortable:deactivate', function(e, ui) {
            var str = '';
            $(this).children('li').each(function() {
            str += this.id + ';';
        });
        $.get($('input[name="intPriority"]').val() +'/'+ str);
    });
});