<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ToDo extends Model
{
    /**
     * Referência para a tabela "todo".
     *
     * @var string
     */
    protected $table = 'todo';

    /**
     * Inclusão dos campos que irão ser utilizados na inserção/alteração.
     *
     * @var array
     */
    protected $fillable = ['title', 'description', 'priority'];

    /**
     * Indicação se haverá campos "timestamped".
     *
     * @var bool
     */
    public $timestamps = false;

    
}
