<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Roteamento para API REST
Route::resource('/todo', 'ToDoController');

//Roteamento específico para a ordenação
Route::get('/order/{ids}', 'ToDoController@getOrder');