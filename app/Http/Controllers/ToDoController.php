<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ToDo;
use App\Http\Requests\ToDoRequest;
use Helper;

class ToDoController extends Controller
{
    private $model;

    /**
     * Construtor referenciando o Model ToDo.
     *
     * @return Object
     */
    public function __construct(){
        $this->model = new ToDo();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = $this->model->orderBy('priority')->get();

        return view('index', compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\ToDoRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ToDoRequest $request)
    {
        $create = $this->model->create(
                                          [
                                             'title'       => $request->title,
                                             'description' => $request->description, 
                                             'priority'    => 0 
                                          ]
                                      );
        $create->save();

        return response('Tarefa criada com sucesso!', 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\ToDoRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ToDoRequest $request, $id)
    {
        $update = $this->model->findOrFail($id);

        $update->update(['title' => $request->title, 'description' => $request->description]);

        return response('Tarefa alterada com sucesso!', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->model->destroy($id);

        return response('Tarefa excluída com sucesso!', 200);
    }

  
    /**
     * Ordenação das tarefas na capa 
     *
     * @param  int  $ids
     * @return \Illuminate\Http\Response
     */
    public function getOrder($ids)
    {
        $ids = array_filter(explode(';', $ids));

        $i = 0;
        foreach ($ids as $id) {
            $class = $this->model->findOrFail($id);
            $class->priority = $i;
            $class->save();
            $i++;
        }
    }
}
