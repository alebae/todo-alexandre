<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>TO DO - Prova Técnica - Alexandre Feustel Baehr</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="{{asset('css/main.css')}}" rel="stylesheet" type="text/css">
        
    </head> 
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                    Listagem de Tarefas                    
                </div>

                @if(count($list))
                    <p>Arraste a tarefa para a ordem desejada!</p>
                    <ul class="sortable">
                        @foreach($list as $data)
                            <li id="{!! $data->id !!}">
                                {!! $data->title !!}
                            </li>
                        @endforeach
                    </ul>
                    {!! Form::hidden('intPriority', url('/order')) !!}
                @else
                    <p>
                        Nenhuma tarefa encontrada.
                    </p>
                @endif
            </div>
            
        </div>

        <script src="{{asset('bower_components/jquery/jquery.min.js')}}"></script>
        <script src="{{asset('bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
        <script src="{{asset('js/sortable.js')}}"></script>
        <script src="{{asset('js/sortable-todo.js')}}"></script>
    </body>
</html>
